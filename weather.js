
var endpoint =`http://api.openweathermap.org/data/2.5/weather?q=${config.city_default}&APPID=${config.api_key}`;

const city = document.querySelector('.city');
const dateLayout = document.querySelector('.date');
const tempLayout = document.querySelector('.temp');
const humidityLayout = document.querySelector('.humidity');
const pressureLayout = document.querySelector('.pressure');
const windSpeedLayout = document.querySelector('.windSpeed');

var convertKelvinToCelcius = function(kelvin) {
  return kelvin - 273.15;
}

var convertMetersSecondsToKilometersHours = function(metersSeconds) {
  return metersSeconds * 3.6;
}

var round = function (value, precision) {
  var multiplier = Math.pow(10, precision || 0);
  return Math.round(value * multiplier) / multiplier;
}

var setTemperature = function (weather, wind) {
  const tempCelcius = round(convertKelvinToCelcius(JSON.stringify(weather.temp)),1);
  const tempHumidity = JSON.stringify(weather.humidity);
  const pressure = JSON.stringify(weather.pressure);
  var windSpeed = convertMetersSecondsToKilometersHours(wind.speed);
  var tempHTML = `<h3>Temperatura actual: ${tempCelcius} Grados</h3>`;
  var humidityHTML = `<h3>Humedad: ${tempHumidity}% </h3>`;
  var presionHTML = `<h3>Presion: ${pressure} hectopascales</h3>`;
  var windHTML = `<h3>Velocidad del viento: ${windSpeed} km/h</h3>`;
  tempLayout.innerHTML = tempHTML;
  humidityLayout.innerHTML = humidityHTML;
  pressureLayout.innerHTML = presionHTML;
  windSpeedLayout.innerHTML = windHTML;
}

var getInfo = function() {
  date = Date();
  dateLayout.innerHTML = `<h3>Horario actual: ${date}</h3>`;
  fetch(endpoint).then((response)=> {
    response.json().then(function(resolved) {
      setTemperature(resolved.main, resolved.wind);
    })
  });
}

city.innerHTML = config.city_default;
getInfo();
setInterval(getInfo,60000);